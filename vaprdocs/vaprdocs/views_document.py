from django.shortcuts import render, render_to_response, redirect, HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.core.exceptions import PermissionDenied
import json
from bson.json_util import dumps
from models import VCabinet, VFolder, VDocument, VFile
import datetime
import mongo
import support


def view_mongo_test(request):
    if not request.user.username:
        return redirect('/accounts/login')

    mng = mongo.get_mongo()
    db = mng.vaprdocs
    cabinets = dumps(db.cabinets.find())

    c = {'user': request.user, 'cabinets': cabinets}
    return render_to_response('mongotest.html', c)


def view_document_new(request):
    if not request.user.username:
        raise PermissionDenied

    c = {'user': request.user}

    if request.method == 'GET':
        return render(request, 'document_new.html', c)

    if request.method == 'POST':
        document = VDocument()
        document.name = request.POST.get('docname')
        document.folder = request.POST.get('docfolder')
        document.cabinet = request.POST.get('doccabinet')
        s = request.POST.get('encodedfile')
        start = s.find('data:') + 5
        end = s.find(';')
        #Content Type = s[start:end]
        #Document Name = request.POST.get('docname')
        #File Data (UTF8) Base64 string = str(s[s.find('base64,')+7:]).encode('utf8')
        fid = mongo.save_file(None, request.POST.get('filename'), s[start:end], str(s[s.find('base64,')+7:]).encode('utf8'), False)
        document.file = VFile(fid)
        document.file_id = fid
        document.created_date = datetime.datetime.now()
        document.created_by = request.user.username
        mongo.save_document(document._document)
        #document.save()
        c['document'] = document
        return render_to_response('document_view.html', c)


def view_document(request, document_id):
    if not request.user.username:
        return redirect('/accounts/login')

    document = VDocument(document_id)
    c = {'user': request.user, 'document': document, 'file': document.file}
    return render_to_response('document_view.html', c)


def view_document_list(request, folder_id):
    if not request.user.username:
        return redirect('/accounts/login')

    c = {'user': request.user, 'documents': mongo.get_documents_by_folder(folder_id)}
    return render_to_response('document_list.html', c)


def view_cabinet(request, cabinet_id):
    if not request.user.username:
        return redirect('/accounts/login')

    cabinet = mongo.get_cabinet(cabinet_id)
    c = {'user': request.user, 'cabinet': cabinet}
    return render_to_response('cabinet_view.html', c)