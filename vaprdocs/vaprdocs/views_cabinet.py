from django.shortcuts import render_to_response, redirect, render
from django.core.exceptions import PermissionDenied
from models import VCabinet

def view_cabinet_list(request):
    if not request.user.username:
        return redirect('/accounts/login')

    c = {'user': request.user}

    return render_to_response('cabinet_list.html', c)


def view_cabinet_new(request):
    if not request.user.username:
        raise PermissionDenied

    c = {'user': request.user}

    if request.method == 'GET':
        return render(request, 'cabinet_new.html', c)

    if request.method == 'POST':
        cabinet = VCabinet()
        cabinet.name = request.POST.get('cabinet_name')
        cabinet.save()
        c['cabinet'] = cabinet
        return render_to_response('cabinet_view.html', c)