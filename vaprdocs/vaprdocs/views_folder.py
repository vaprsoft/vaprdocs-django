from django.shortcuts import render, render_to_response, redirect, HttpResponse, Http404, RequestContext
from django.views.decorators.csrf import csrf_protect
from django.core.exceptions import PermissionDenied
from models import VCabinet, VFolder
import support


#@csrf_protect
def view_folder_new(request):
    if not request.user.username:
        raise PermissionDenied

    #cp = support.split_cabinet_path(path)
    #cabinet = VCabinet(cp[0])

    if request.method == 'GET':
        c = {'user': request.user}
        return render(request, 'folder_new.html', c)
    if request.method == 'POST':
        folder = VFolder()
        folder.name = request.POST.get('new_folder_name')
        folder.cabinet = request.POST.get('new_folder_cabinet')
        folder.parent = request.POST.get('new_folder_parent')
        folder.save()
        c = {'user': request.user}
        return render_to_response('modal_new_close.html', c)


def view_folder_list(request, cabinet, folder=None):
    if not request.user.username:
        raise PermissionDenied
    c = {'user': request.user}

    if cabinet and cabinet != 'undefined':
        c['cabinet'] = VCabinet(cabinet)

    if folder and folder != 'undefined':
        c['folder'] = VFolder(folder)

    return render_to_response('folder_list.html', c)