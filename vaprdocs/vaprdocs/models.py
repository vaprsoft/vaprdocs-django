import datetime
import base64
import mongo


class VCabinet(object):
    _cabinet = None

    @property
    def id(self):
        return self._cabinet['_id']

    @property
    def name(self):
        return self._cabinet['name']

    @name.setter
    def name(self, value):
        self._cabinet['name'] = value

    @property
    def user(self):
        return self._cabinet['user']

    @user.setter
    def user(self, value):
        self._cabinet['user'] = value

    @property
    def group(self):
        return self._cabinet['group']

    @group.setter
    def group(self, value):
        self._cabinet['group'] = value

    def __init__(self, cid=None):
        if cid is None:
            self._cabinet = {}
        else:
            self._cabinet = mongo.get_cabinet(cid)

    def load(self, cid):
        self.__init__(cid)

    def save(self):
        mongo.save_cabinet(self._cabinet)

    def delete(self):
        mongo.delete_cabinet(self._cabinet)


class VFolder(object):
    _folder = None

    @property
    def id(self):
        return self._folder['_id']

    @property
    def name(self):
        return self._folder.get('name')

    @name.setter
    def name(self, value):
        self._folder['name'] = value

    @property
    def parent(self):
        return self._folder.get('parent')

    @parent.setter
    def parent(self, value):
        self._folder['parent'] = value

    @property
    def cabinet(self):
        return self._folder['cabinet']

    @cabinet.setter
    def cabinet(self, value):
        self._folder['cabinet'] = value

    @property
    def user(self):
        return self._folder['user']

    @user.setter
    def user(self, value):
        self._folder['user'] = value

    @property
    def group(self):
        return self._folder['group']

    @group.setter
    def group(self, value):
        self._folder['group'] = value

    def __init__(self, fid=None):
        if fid is None:
            self._folder = {}
        else:
            self._folder = mongo.get_folder(fid)

    def load(self, fid):
        self.__init__(fid)

    def save(self):
        mongo.save_folder(self._folder)

    def delete(self):
        mongo.delete_folder(self._folder)


class VDocument(object):
    _document = {}
    file = None

    @property
    def id(self):
        return self._document['_id']

    @property
    def name(self):
        return self._document['name']

    @name.setter
    def name(self, value):
        self._document['name'] = value

    @property
    def cabinet(self):
        return self._document['cabinet']

    @cabinet.setter
    def cabinet(self, value):
        self._document['cabinet'] = value

    @property
    def folder(self):
        return self._document['folder']

    @folder.setter
    def folder(self, value):
        self._document['folder'] = value

    @property
    def file_id(self):
        return self._document['file_id']

    @file_id.setter
    def file_id(self, value):
        self._document['file_id'] = value

    @property
    def created_by(self):
        return self._document['created_by']

    @created_by.setter
    def created_by(self, value):
        self._document['created_by'] = value

    @property
    def modified_by(self):
        return self._document['modified_by']

    @modified_by.setter
    def modified_by(self, value):
        self._document['modified_by'] = value

    @property
    def created_date(self):
        return self._document['created_date']

    @created_date.setter
    def created_date(self, value):
        self._document['created_date'] = value

    @property
    def modified_date(self):
        return self._document['modified_date']

    @modified_date.setter
    def modified_date(self, value):
        self._document['modified_date'] = value

    @property
    def fields(self):
        return self._document['fields']

    @fields.setter
    def fields(self, value):
        self._document['fields'] = value

    def __init__(self, did=None):
        if did is None:
            self._document = {}
            self.file = VFile()
        else:
            self._document = mongo.get_document(did)
            self.file = VFile(self._document.get('file_id', None))
            self.file_id = self.file.id

    def load(self, did):
        self.__init__(did)

    def save(self):
        self.file.save()
        self.file_id = self.file.id
        self._document._id = mongo.save_document(self._document)

    def delete(self):
        mongo.delete_file(self.file_id)
        mongo.delete_document(self._document)


class VFile(object):
    _file = None
    _data = None
    _update = False

    @property
    def id(self):
        return str(self._file._id)

    @property
    def filename(self):
        return self._file.filename

    @filename.setter
    def filename(self, value):
        self._file.filename = value

    @property
    def content_type(self):
        return self._file.content_type

    @content_type.setter
    def content_type(self, value):
        self._file.content_type = value

    @property
    def new_data(self):
        return self._data

    @new_data.setter
    def new_data(self, base64_encoded_value):
        self._update = True
        self._data = base64_encoded_value

    @property
    def data(self):
        return 'data:'+self._file.content_type+';base64,'+mongo.read_file(self.id)

    def __init__(self, fid=None):
        if fid is None:
            self._file = None
        else:
            self._file = mongo.get_file(fid)
        return

    def save(self):
        tid = mongo.save_file(id, self.filename, self.content_type, self._data, self._update)
        self._update = False
        self._file = mongo.get_file(tid)