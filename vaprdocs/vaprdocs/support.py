def split_cabinet_path(path):
    elements = path.split('/')
    path = ''
    if len(elements) > 0:
        if len(elements) > 1:
            for e in elements[1:]:
                path += '/'+e

    return [str(elements[0]), path]