from django.shortcuts import render_to_response, redirect, HttpResponse
from bson.json_util import dumps
from bson.objectid import ObjectId
import mongo
import models
import response
import json


def view_json_get_cabinet_list(request):
    if not request.user.username:
        return response.response_unauthorized()

    mng = mongo.get_mongo()
    db = mng.vaprdocs
    result = []
    for cabinet in db.cabinets.find():
        result.append({'name': cabinet['name'], 'id': str(cabinet['_id'])})

    #cabinets = dumps(db.cabinets.find())

    return HttpResponse(json.dumps(result), 'application/json')


def view_json_get_cabinet_by_id(request, cabinet_id):
    if not request.user.username:
        return response.response_unauthorized()

    cabinet = models.VCabinet(cid=cabinet_id)
    result = {'id': str(cabinet.id), 'name': cabinet.name}
    return HttpResponse(json.dumps(result), 'application/json')


def view_json_get_folder_by_id(request, folder_id):
    if not request.user.username:
        return response.response_unauthorized()

    folder = models.VFolder(fid=folder_id)
    result = {'id': str(folder.id), 'name': folder.name, 'parent': folder.parent}
    return HttpResponse(json.dumps(result), 'application/json')


def view_json_get_folder_list(request, path):
    if not request.user.username:
        return response.response_unauthorized()

    elements = path.split('/')
    cabinet_id = elements[0]
    parent = None
    if len(elements) > 1:
        if elements[1] != 'undefined':
            parent = elements[1]

    folders = mongo.get_folder_list(cabinet_id, parent)

    result = []

    for folder in folders:
        result.append({'id': str(folder['_id']), 'name': folder['name'], 'parent': folder['parent']})

    return HttpResponse(json.dumps(result), 'application/json')


def view_json_folder_path(request, folder_id):
    if not request.user.username:
        return response.response_unauthorized()

    path = mongo.get_folder_path(folder_id)
    return HttpResponse(path, 'text/html')


def view_json_get_document_list(request, folder_id):
    if not request.user.username:
        return response.response_unauthorized()

    #elements = path.split('/')
    #cabinet_id = elements[0]
    #path = '/'
    #if len(elements) > 1:
    #    path = '/'+path.join(elements[1:])+'/'

    #folder = mongo.get_folder_by_path(cabinet_id, path)

    documents = mongo.get_documents_by_folder(folder_id)

    result = []

    for document in documents:
        result.append({'id': str(document['_id']), 'name': document.get('name'), 'cabinet': document.get('cabinet'),
                       'folder': document.get('folder'), 'file': document.get('file'),
                       'created_date': str(document.get('created_date')), 'created_by': document.get('created_by')})

    return HttpResponse(json.dumps(result), 'application/json')


def view_json_delete_document(request, document_id):
    if not request.user.username:
        return response.response_unauthorized()

    mongo.delete_document(document_id)
    return HttpResponse(json.dumps(True), 'application/json')
