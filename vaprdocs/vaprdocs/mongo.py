from pymongo import MongoClient
import gridfs
from bson.objectid import ObjectId
import re
import gridfs


def get_mongo():
    return MongoClient()


def get_mongo_db():
    mng = get_mongo()
    return mng.vaprdocs


def get_folder_path(folder_id):
    db = get_mongo_db()
    folder = db.folders.find_one({'_id': ObjectId(folder_id)})
    path = folder.get('name')
    pr = folder.get('parent', '')
    if pr != '':
        parent = db.folders.find_one({'_id': ObjectId(pr)})
    else:
        parent = ''
    while parent is not '':
        path = parent.get('name')+'/'+path
        pr = parent.get('parent', '')
        if pr == '':
            parent = ''
        else:
            parent = db.folders.find_one({'_id': ObjectId(pr)})
    return path


def get_cabinet(cabinet_id):
    db = get_mongo_db()
    cabinet = db.cabinets.find({'_id': ObjectId(cabinet_id)})[0]
    return cabinet


def get_folder(folder_id):
    db = get_mongo_db()
    #folder = db.folders.find({'_id': ObjectId(folder_id)})[0]
    folder = db.folders.find_one({'_id': ObjectId(folder_id)})
    return folder


def get_folder_by_path(cabinet_id, path):
    db = get_mongo_db()

    if path == '':
        return None
    if path[0] == '/':
        path = path[1:]
    if path[-1] == '/':
        path = path[0:-1]
    elements = path.split('/')
    path = '/'
    if len(elements) > 1:
        path = '/'+path.join(elements[0:-1])+'/'
    name = elements[-1]
    print(path)
    print(name)
    folder = db.folders.find_one({'cabinet': cabinet_id, 'path': path.lower(), 'name': name})
    return folder


def get_folder_list(cabinet_id, parent):
    db = get_mongo_db()
    folders = []
    if not parent:
        folders = db.folders.find({'cabinet': cabinet_id, 'parent': ''})
    else:
        folders = db.folders.find({'cabinet': cabinet_id, 'parent': parent})
    return folders


def get_document(document_id):
    db = get_mongo_db()
    document = db.documents.find({'_id': ObjectId(document_id)})[0]
    return document


def get_documents_by_folder(folder_id):
    db = get_mongo_db()
    documents = db.documents.find({'folder': folder_id})
    return documents


def save_cabinet(cabinet):
    db = get_mongo_db()
    db.cabinets.update({'_id': cabinet.get('_id')}, cabinet, True)


def save_folder(folder):
    db = get_mongo_db()
    if folder.get('_id', None) is None:
        db.folders.insert(folder)
        #db.folders.update({'_id': None}, folder, True)
    else:
        db.folders.update({'_id': folder.get('_id')}, folder, True)


def save_document(document):
    db = get_mongo_db()
    if document.get('_id', None) is None:
        return str(db.documents.save(document, manipulate=True))
    else:
        return str((db.documents.update({'_id': document.get('_id')}, document, True)).get('_id'))


def get_file(file_id):
    file_id = ObjectId(file_id)
    db = get_mongo_db()
    fs = gridfs.GridFS(db)
    if fs.exists(file_id):
        return fs.get(file_id)
    else:
        return None


def read_file(file_id):
    file_id = ObjectId(file_id)
    db = get_mongo_db()
    fs = gridfs.GridFS(db)
    if not fs.exists(file_id):
        return None
    else:
        return fs.get(file_id).read()


def save_file(file_id, filename, mimetype, data, update=False):
    db = get_mongo_db()
    fs = gridfs.GridFS(db)
    kwargs = dict(filename=filename, content_type=mimetype)

    if file_id is not None and fs.exists(file_id):
        if update is False:
            return file_id
        else:
            kwargs['_id'] = file_id
            fs.delete(file_id)

    return fs.put(data, **kwargs)


def delete_file(file_id):
    db = get_mongo_db()
    fs = gridfs.GridFS(db)

    if fs.exists(file_id):
        fs.delete(file_id)


def delete_document(document_id):
    db = get_mongo_db()
    doc = db.documents.find_one({'_id': ObjectId(document_id)})
    f = doc.get('file_id', None)
    if f is not None:
        delete_file(f)
    db.documents.remove({'_id': ObjectId(document_id)})