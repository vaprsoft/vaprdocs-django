from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'vaprdocs.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^cabinets/', 'vaprdocs.views_cabinet.view_cabinet_list'),
    url(r'^cabinet/new', 'vaprdocs.views_cabinet.view_cabinet_new'),
    url(r'^folders/(?P<cabinet>.+)/$', 'vaprdocs.views_folder.view_folder_list'),
    url(r'^folders/(?P<cabinet>.+)/(?P<folder>.+)/$', 'vaprdocs.views_folder.view_folder_list'),
    url(r'^folder/new/', 'vaprdocs.views_folder.view_folder_new'),
    url(r'^documents/(?P<folder_id>.+)/$', 'vaprdocs.views_document.view_document_list'),
    url(r'^document/new/', 'vaprdocs.views_document.view_document_new'),
    url(r'^document/view/(?P<document_id>.+)/$', 'vaprdocs.views_document.view_document'),
    url(r'^api/cabinet/list/', 'vaprdocs.views_json.view_json_get_cabinet_list'),
    url(r'^api/cabinet/id/(?P<cabinet_id>.+)/$', 'vaprdocs.views_json.view_json_get_cabinet_by_id'),
    url(r'^api/folder/list/(?P<path>.+)/$', 'vaprdocs.views_json.view_json_get_folder_list'),
    url(r'^api/folder/id/(?P<folder_id>.+)/$', 'vaprdocs.views_json.view_json_get_folder_by_id'),
    url(r'^api/folder/path/(?P<folder_id>.+)/$', 'vaprdocs.views_json.view_json_folder_path'),
    url(r'^api/document/list/(?P<folder_id>.+)/$', 'vaprdocs.views_json.view_json_get_document_list'),
    url(r'^api/document/delete/(?P<document_id>.+)/$', 'vaprdocs.views_json.view_json_delete_document'),
    url(r'^$', 'vaprdocs.views.view_index', name='view_index')
)
