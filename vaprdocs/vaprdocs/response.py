from django.shortcuts import HttpResponse


def response_unauthorized():
    res = HttpResponse("Unauthorized")
    res.status_code = 401
    return res