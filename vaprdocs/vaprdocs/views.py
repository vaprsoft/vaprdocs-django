from django.shortcuts import render_to_response, redirect

def view_index(request):
    if not request.user.username:
        return redirect('/accounts/login')

    c = {'user': request.user}
    return render_to_response('index.html', c)
